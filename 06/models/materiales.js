// const Persona = require("./persona");

class Materiales {
  constructor() {
    this._listado = [];
  }

  // Método para crear persona
  crearMaterial(material = {}) {
    // Del objeto json de materiales obtenemos el id y lo guardamos en el arreglo de _listado = []
    this._listado[material.id] = material;
  }

  // Getter para obtener el listado de materiales registradas
  get listArray() {
    // aca almacenaremos cada material
    const listado = [];
    // recorremos el objeto de json de personas
    Object.keys(this._listado).forEach(key => {
      // cada registro de persona del listado lo almacenamos en persona
      const material = this._listado[key];
      // guardamos cada material en listado
      listado.push(material);
    })
    // retornamos el listado de personas
    return listado;
  }


  // Método para mostrar la lista de materiales
  cargarMaterialesFromArray(personas = []) {
    // Del objeto json de materiales obtenemos el id y lo guardamos en el arreglo de _listado = []
    personas.forEach(persona => {
      this._listado[persona.id] = persona;
    })
  }


  // Método para eliminar una persona
  eliminarMaterial(id = '') {
    if (this._listado[id]) {
      delete this._listado[id];
    }
  }

}

module.exports = Materiales