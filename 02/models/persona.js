// Obtenemos el paquete de uuid para los ids
const { v4: uuidv4 } = require('uuid');

class Persona {
  id = '';
  nombres = ''; 
  apellidos = '';
  ci = 0;
  direccion = '';
  sexo = '';

  constructor(nombres, apellidos, ci, direccion, sexo) {
    this.id = uuidv4();
    this.nombres = nombres;
    this.apellidos = apellidos;
    this.ci = ci;
    this.direccion = direccion;
    this.sexo = sexo;
  }

  getId(idx) {
    this.id = idx
  }


/* let findCityLondon = users.find((user) => {
  try {
    return user.address.city.includes('London');
  } catch {
    return false
  }
});
console.log(findCityLondon);

let findId1user = users.find(user => user.id == 1)
console.log(findId1user) */

  /* find(ci) {
    personas.find(persona => persona.ci === ci)
    
  } */
}


module.exports = Persona;